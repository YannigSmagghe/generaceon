# Base du projet
Faire une course avec un vaisseau spatial qui relie 4 checkpoint en 3 tours.

# Function Fitness
- Fait d'y arriver 
- Passer la ligne d'arrivée en moins de temps possible

# Outputs
- Vitesse cible du vaisseau 0-100
- Angle du vaisseau (pour le faire tourner)

# Inputs
- Vitesse actuelle
- Résultats des capteurs
- 

# Generace On Bot
## Type of sensors
- Raycast sur une zone sensible au checkpoint a viser qui renvoie
		- No hit : distance infinie
		- Hit : distance du checkpoint
-   

## Source

https://www.codingame.com/ide/puzzle/coders-strike-back

Faire ce tuto pour voir de quoi on parle
