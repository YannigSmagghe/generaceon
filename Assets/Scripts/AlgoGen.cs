﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlgoGen : MonoBehaviour
{

    public List<Transform> listCheckpoints = new List<Transform>();
	public LineRenderer raceLine;
    public float generationTime = 30f;
    public int populationSize = 10;
    public int numGenMax = 50;
    public GameObject prefabSpaceShip;
    public float bestFitness;
    private SpaceshipBrain bestBrain;
    private SpaceshipBrain previousBestBrain;
    public float averageFitness;
    public int numGen = 0;

	private int minimumSelectionNb;
    private float timer = 0.0f;
    private List<AutoPilot> ships = new List<AutoPilot>();

    private List<SpaceshipBrain> brains = new List<SpaceshipBrain>();
    private List<SpaceshipBrain> brainsChildTemp = new List<SpaceshipBrain>();

    // Use this for initialization
    void Start()
    {
		raceLine.positionCount = listCheckpoints.Count;
		for (int i = 0; i < listCheckpoints.Count; i++)
			raceLine.SetPosition(i, listCheckpoints[i].position);

        for (int i = 0; i < populationSize; i++)
        {
            GameObject go = Instantiate(prefabSpaceShip, transform.position, Quaternion.identity, this.transform);
            go.GetComponent<RaceRun>().Init(listCheckpoints);
            go.transform.rotation *= Quaternion.Euler(new Vector3(0,0,135));

            AutoPilot ship = go.GetComponent<AutoPilot>();
            ships.Add(ship);

            SpaceshipBrain brain = new SpaceshipBrain();
            brain.RandomInit();
            brains.Add(brain);

            ship.brain = brain;
        }

		minimumSelectionNb = (int)(populationSize * 0.2f);

        UnPause();
    }

    //MUTE LES INDIVIDUS AVEC  5% DE CHANCE
    void Mutation()
    {
        foreach (SpaceshipBrain brain in brains)
        {
            if (UnityEngine.Random.Range(0f, 100f) < 0.05f)
            {
                brain.Mutation();
            }
        }
    }

	//CREE LES ENFANTS A PARTIR DE PARENTS TIRES ALEATOIREMENT
	#region ancienneHybridation
	//  void Hybridation()
	//  {
	//int childNeeded = populationSize - brains.Count;
	//      brainsChildTemp.Clear();
	//      while (brainsChildTemp.Count < childNeeded)
	//      {
	//          SpaceshipBrain spaceship = new SpaceshipBrain();
	//          spaceship = new SpaceshipBrain();
	//          int father = UnityEngine.Random.Range(0, brains.Count);
	//          int mother;
	//          do
	//          {
	//              mother = UnityEngine.Random.Range(0, brains.Count);
	//          } while (mother == father);
	//          spaceship.Hybridation(brains[father], brains[mother]);

	//          brainsChildTemp.Add(spaceship);
	//      }

	//      brains.AddRange(brainsChildTemp);
	//  }
	#endregion

	void Hybridation()
	{
		int childNeeded = populationSize - brains.Count;
		brainsChildTemp.Clear();
		while (brainsChildTemp.Count < childNeeded)
		{
			SpaceshipBrain spaceship = new SpaceshipBrain();
			spaceship = new SpaceshipBrain();
			int father = weightedRandom();
			int mother;
			do
			{
				mother = weightedRandom();
			} while (mother == father);
			spaceship.Hybridation(brains[father], brains[mother]);

			brainsChildTemp.Add(spaceship);
		}

		brains.AddRange(brainsChildTemp);
	}

	int weightedRandom()
	{
		float chancePart = 1.0f / ((float)brains.Count * ((float)brains.Count + 1.0f) / 2.0f);
		int partsToAdd = 2;
		float chances = chancePart;

		float result = UnityEngine.Random.Range(0.0f, 1.0f);

		for (int i = brains.Count-1; i >= 0; i--)
			if (result <= chances)
				return i;
			else
				chances += chancePart * partsToAdd++;

		return -1;
	}

	//ON GARDE LES 50% MEILLEURS
	#region ancienneSelection
	//void Selection()
	//{
	//    previousBestBrain = bestBrain;
	//    List<SpaceshipBrain> bestShips = new List<SpaceshipBrain>();
	//    bestFitness = 0;
	//    averageFitness = 0;
	//    foreach (SpaceshipBrain brain in brains)
	//    {
	//        if (brain.fitness > bestFitness)
	//        {
	//            bestFitness = brain.fitness;
	//            bestBrain = brain;
	//        }
	//        averageFitness += brain.fitness;

	//        bestShips.Add(brain);
	//        if (bestShips.Count > populationSize / 2)
	//        {
	//            float minFitness = float.MaxValue;
	//            SpaceshipBrain worst = null;
	//            foreach (SpaceshipBrain temp in bestShips)
	//            {
	//                if (temp.fitness <= minFitness)
	//                {
	//                    worst = temp;
	//                    minFitness = worst.fitness;
	//                }
	//            }

	//            bestShips.Remove(worst);
	//        }
	//    }
	//    averageFitness /= populationSize;

	//    brains = new List<SpaceshipBrain>(bestShips);


	//}
	#endregion

	void Selection()
	{
		List<SpaceshipBrain> selectedShips = new List<SpaceshipBrain>();

		previousBestBrain = bestBrain;
		bestFitness = 0;
		SortBrains();
		bestFitness = brains[0].fitness;
		bestBrain = brains[0];

		averageFitness = 0;
		foreach (SpaceshipBrain brain in brains)
			averageFitness += brain.fitness;
		averageFitness /= populationSize;

		foreach (SpaceshipBrain brain in brains)
			if (selectedShips.Count < minimumSelectionNb || brain.fitness >= averageFitness)
				selectedShips.Add(brain);

		brains = selectedShips;
	}

	// Update is called once per frame
	void Update()
    {

        timer += Time.deltaTime;

        if (timer > generationTime)
        {
            Pause();
            Selection();
            Hybridation();
            Mutation();
            numGen++;

            AttributeBrains();

            Reset();
        }

    }

    private void AttributeBrains()
    {

        for (int i = 0; i < populationSize; i++)
        {
            ships[i].GetComponent<SpriteRenderer>().color = Color.white;
            if (brains[i] == previousBestBrain) ships[i].GetComponent<SpriteRenderer>().color = new Color(255, 0, 255);
            if (brains[i] == bestBrain) ships[i].GetComponent<SpriteRenderer>().color = new Color(0, 255, 255);
            ships[i].brain = brains[i];
        }
    }

    void Reset()
    {
        foreach (var ship in ships)
        {
            ship.transform.position = transform.position;
            ship.transform.rotation = Quaternion.identity;
            ship.brain.fitness = 0;
            ship.GetComponent<RaceRun>().Reset();
            ship.transform.rotation *= Quaternion.Euler(new Vector3(0, 0, 135));
            ship.GetComponent<Rigidbody2D>().velocity = prefabSpaceShip.GetComponent<Rigidbody2D>().velocity;
            ship.GetComponent<Rigidbody2D>().angularVelocity = prefabSpaceShip.GetComponent<Rigidbody2D>().angularVelocity;
        }
        UnPause();
        timer = 0.0f;
    }

	public void SortBrains()
	{
		List<SpaceshipBrain> newList = new List<SpaceshipBrain>();

		while(brains.Count > 0)
		{
			int idBest = 0;
			for (int i = 0; i < brains.Count; i++)
				if (brains[i].fitness > brains[idBest].fitness)
					idBest = i;
			newList.Add(brains[idBest]);
			brains.Remove(brains[idBest]);
		}

		brains = newList;
	}

	void Pause()
    {
        foreach (AutoPilot ship in ships)
        {
            ship.Pause();
            ship.Fitness();
        }
    }
    void UnPause()
    {
        foreach (AutoPilot ship in ships)
        {
            ship.UnPause();
        }
    }
}
