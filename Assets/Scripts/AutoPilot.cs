﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpaceshipControl), typeof(RaceRun))]
public class AutoPilot : MonoBehaviour {

	public SpaceshipBrain brain;
    [HideInInspector]
    public Rigidbody2D rb;

    private bool stop = true;

    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

	void Update () {
        if (!stop)
        {
            List<float> inputs = GenerateInputs();
            List<float> outputs = brain.Process(new SpaceshipBrainInputs(inputs)).AsList();
            InterpretOutputs(outputs);
        }
	}

	public List<float> GenerateInputs()
	{
		List<float> inputs = new List<float>();

		Vector3 target = GetComponent<RaceRun>().destination;

		float angle = Vector3.Angle(-transform.up, target-transform.position);
		float side = Vector3.Dot(target - transform.position, -transform.right);
		side = side / Mathf.Abs(side);
		angle = angle * side;

		inputs.Add(rb.velocity.magnitude);
        inputs.Add(Vector3.Angle(transform.forward, rb.velocity));

        if (angle >= -63 && angle < -38)
		{
			inputs.Add(Vector3.Distance(transform.position, target));
			inputs.Add(float.MaxValue);
			inputs.Add(float.MaxValue);
			inputs.Add(float.MaxValue);
			inputs.Add(float.MaxValue);
		}
		else if (angle >= -38 && angle < -13)
		{
			inputs.Add(float.MaxValue);
			inputs.Add(Vector3.Distance(transform.position, target));
			inputs.Add(float.MaxValue);
			inputs.Add(float.MaxValue);
			inputs.Add(float.MaxValue);
		}
		else if (angle >= -13 && angle < 13)
		{
			inputs.Add(float.MaxValue);
			inputs.Add(float.MaxValue);
			inputs.Add(Vector3.Distance(transform.position, target));
			inputs.Add(float.MaxValue);
			inputs.Add(float.MaxValue);
		}
		else if (angle >= 13 && angle < 38)
		{
			inputs.Add(float.MaxValue);
			inputs.Add(float.MaxValue);
			inputs.Add(float.MaxValue);
			inputs.Add(Vector3.Distance(transform.position, target));
			inputs.Add(float.MaxValue);
		}
		else if (angle >= 38 && angle < 63)
		{
			inputs.Add(float.MaxValue);
			inputs.Add(float.MaxValue);
			inputs.Add(float.MaxValue);
			inputs.Add(float.MaxValue);
			inputs.Add(Vector3.Distance(transform.position, target));
		}
		else
		{
			inputs.Add(float.MaxValue);
			inputs.Add(float.MaxValue);
			inputs.Add(float.MaxValue);
			inputs.Add(float.MaxValue);
			inputs.Add(float.MaxValue);
		}
        
		return inputs;
	}

	public void InterpretOutputs(List<float> outputs)
	{
		GetComponent<SpaceshipControl>().SetTargetSpeed(outputs[0]);
		GetComponent<SpaceshipControl>().SetTargetAngle(outputs[1]);
	}

    public void Pause()
    {
        stop = true;
    }

    public void UnPause()
    {
        stop = false;
    }

    internal void Fitness()
    {
        Vector3 destination = GetComponent<RaceRun>().destination;
        Vector3 previousCP = GetComponent<RaceRun>().previous;

        if (Vector3.Distance(destination, transform.position) < Vector3.Distance(destination, previousCP))
        {
            brain.fitness += 1 - (Vector3.Distance(destination, transform.position) / Vector3.Distance(destination, previousCP));
        }
    

    }
}
