﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LapTimeManager : MonoBehaviour {

    //UI Timer
    public static int MinuteCount;
    public static int SecondCount;
    public static float Millicount;
    public static string MilliDisplay;

    public GameObject MinuteBox;
    public GameObject SecondBox;
    public GameObject MilliBox;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Millicount += Time.deltaTime * 10;
        MilliDisplay = (Millicount*10).ToString("F0");
        MilliBox.GetComponent<Text>().text = "" + MilliDisplay;

        if (Millicount >= 10)
        {
            Millicount = 0;
            SecondCount += 1;
        }

        if (SecondCount <= 9)
        {
            SecondBox.GetComponent<Text>().text = "0" + SecondCount.ToString() + ".";
        }
        else
        {
            SecondBox.GetComponent<Text>().text = "" + SecondCount.ToString() + ".";
        }


        if (SecondCount >= 60)
        {
            MinuteCount += 1;
            SecondCount = 0;
        }
        if (MinuteCount <= 9)
        {
            MinuteBox.GetComponent<Text>().text = "0" + MinuteCount.ToString() + ":";
        }
        else
        {
            MinuteBox.GetComponent<Text>().text = "" + MinuteCount.ToString() + ":";
        }
    }
}
