﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RaceManager : MonoBehaviour {

    public int numberOfLap = 3;
    public int currentLap = 0;
    public float Timer = 0;

    // Use this for initialization
    void Start () {
		
	}

	// Update is called once per frame
	void Update () {

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Spaceship")
        {
            currentLap++;
            if (currentLap > 3)
            {
                Debug.Log("Fin de la course !");
            }
        }
    }

}
