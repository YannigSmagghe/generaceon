﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardControl : MonoBehaviour {

    private SpaceshipControl spaceship;

	// Use this for initialization
	void Start () {
        spaceship = GetComponent<SpaceshipControl>();
	}
	
	// Update is called once per frame
	void Update () {
        spaceship.SetTargetSpeed(Input.GetAxis("Vertical"));
        spaceship.SetTargetAngle(Input.GetAxis("Horizontal"));
	}
}
