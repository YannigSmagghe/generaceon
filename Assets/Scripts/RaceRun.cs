﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaceRun : MonoBehaviour {

    [SerializeField]
    public List<Transform> listCheckpoints;

    private int currentCheckpoint;
    public Vector3 destination;
    public Vector3 previous;
	
	public void Init (List<Transform> _listCheckpoints) {        
        listCheckpoints = _listCheckpoints;
        Reset();
    }

    public void Reset()
    {
        previous = transform.position;
        currentCheckpoint = 0;
        destination = listCheckpoints[0].position;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "checkpoints")
        {
            if (other.transform.position == destination)
            {
                gameObject.GetComponent<AutoPilot>().brain.fitness++;
                currentCheckpoint++;                
                if (currentCheckpoint > listCheckpoints.Count-1)
                {
                    currentCheckpoint = 0;
                }
                previous = destination;
                destination = listCheckpoints[currentCheckpoint].position;
            }
        }
    }


}
