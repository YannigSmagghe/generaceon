﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceshipBrain
{

	public NeuronNetwork nn;

	public float fitness = 0;

	public SpaceshipBrain()
	{
		nn.layers = new List<NeuronLayer>();
	}

	public void RandomInit()
	{
		//Hidden layer 1
		int nHidden1 = Mathf.CeilToInt((SpaceshipBrainInputs.nInputs + SpaceshipBrainOutputs.nOutputs) / 2.0f);
		NeuronLayer hiddenLayer1 = NeuralNetworkManager.RandomNeuronLayer(SpaceshipBrainInputs.nInputs, nHidden1);

		//Hidden layer 2
		int nHidden2 = Mathf.CeilToInt((nHidden1 + SpaceshipBrainOutputs.nOutputs) / 2.0f);
		NeuronLayer hiddenLayer2 = NeuralNetworkManager.RandomNeuronLayer(nHidden1, nHidden2);

		//Output layer
		NeuronLayer outputLayer = NeuralNetworkManager.RandomNeuronLayer(nHidden2, SpaceshipBrainOutputs.nOutputs);

		nn.layers.Add(hiddenLayer1);
		nn.layers.Add(hiddenLayer2);
		nn.layers.Add(outputLayer);

	}

	public void Mutation()
	{
		int nMutations = Random.Range(1, 4);

		for (int i = 0; i < nMutations; i++)
		{
			int numLayer = Random.Range(0, nn.layers.Count);
			int numParam = Random.Range(0, nn.layers[numLayer].neuronParams.Count);
			int numWeight = Random.Range(0, nn.layers[numLayer].neuronParams[numParam].weightInputs.Count);

			nn.layers[numLayer].neuronParams[numParam].weightInputs[numWeight] = NeuralNetworkManager.RandomWeigthChange(nn.layers[numLayer].neuronParams[numParam].weightInputs[numWeight]);
		}
	}

	#region ancienneHybridation
	//public void Hybridation(SpaceshipBrain dad, SpaceshipBrain mom)
	//{
	//	for (int l = 0; l < dad.nn.layers.Count; l++)
	//	{
	//		NeuronLayer layer;
	//		layer.neuronParams = new List<NeuronParam>();

	//		for (int p = 0; p < dad.nn.layers[l].neuronParams.Count; p++)
	//		{
	//			NeuronParam param;
	//			param.sigmoidParam = 2f;
	//			param.weightInputs = new List<float>();

	//			for (int w = 0; w < dad.nn.layers[l].neuronParams[p].weightInputs.Count; w++)
	//			{
	//				if (Random.Range(0, 2) == 0)
	//					param.weightInputs.Add(dad.nn.layers[l].neuronParams[p].weightInputs[w]);
	//				else
	//					param.weightInputs.Add(mom.nn.layers[l].neuronParams[p].weightInputs[w]);
	//			}
	//			layer.neuronParams.Add(param);
	//		}

	//		nn.layers.Add(layer);
	//	}
	//}
	#endregion

	public void Hybridation(SpaceshipBrain dad, SpaceshipBrain mom)
	{
		for (int l = 0; l < dad.nn.layers.Count; l++)
		{
			NeuronLayer layer;
			layer.neuronParams = new List<NeuronParam>();

			for (int p = 0; p < dad.nn.layers[l].neuronParams.Count; p++)
			{
				NeuronParam param;
				param.activationParam = NeuralNetworkManager.maxActivationParam;
				param.weightInputs = new List<float>();

				for (int w = 0; w < dad.nn.layers[l].neuronParams[p].weightInputs.Count; w++)
				{
					param.weightInputs.Add(
						(dad.nn.layers[l].neuronParams[p].weightInputs[w] + mom.nn.layers[l].neuronParams[p].weightInputs[w]) / 2.0f
					);
				}
				layer.neuronParams.Add(param);
			}

			nn.layers.Add(layer);
		}
	}

	public SpaceshipBrainOutputs Process(SpaceshipBrainInputs inputs)
	{
		List<float> outputsF = NeuralNetworkManager.NeuronNetworkCalculus(nn, inputs.AsList());

		return new SpaceshipBrainOutputs(outputsF);
	}

}