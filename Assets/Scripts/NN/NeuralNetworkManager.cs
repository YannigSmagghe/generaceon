﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class NeuralNetworkManager
{
    //OFFSET INPUT TO ADD AT EACH LAYER
    public static float offset = 1;
	public static float maxActivationParam = 5.0f;

    //NEURAL NETWORK PROCESSING
    public static List<float> NeuronNetworkCalculus(NeuronNetwork network, List<float> inputs)
    {
        List<float> outputs = inputs;

        foreach (NeuronLayer layer in network.layers)
        {
            outputs = NeuronLayerCalculus(layer, outputs);
        }

        return outputs;
    }

    public static List<float> NeuronLayerCalculus(NeuronLayer layer, List<float> inputs)
    {
        List<float> outputs = new List<float>();

        foreach(NeuronParam param in layer.neuronParams)
        {
            outputs.Add(ActivateNeuronFunction(param, inputs));
        }

        return outputs;
    }

    public static float ActivateNeuronFunction(NeuronParam param, List<float> inputs)
    {

        if(param.weightInputs.Count != inputs.Count + 1) //+1 for offset input
        {
            Debug.LogError("ERROR using neuronal network");
        }

        float result = offset * param.weightInputs[0];

        for (int i = 0; i < inputs.Count; ++i)
        {
            result += inputs[i] * param.weightInputs[i + 1];
        } 

        result = Mathf.Exp(-2 * param.activationParam * result);
        result = 2 / (1 + result) -1;

        return result;
    }

    //RANDOM INIT VALUES
    public static NeuronLayer RandomNeuronLayer(int nInputs, int nOutputs)
    {
        NeuronLayer layer;
        layer.neuronParams = new List<NeuronParam>();

        for (int i = 0; i < nOutputs; i++)
        {
            layer.neuronParams.Add(RandomNeuronParam(nInputs));
        }

        return layer;
    }

    public static NeuronParam RandomNeuronParam(int nInputs)
    {
        NeuronParam neuronParam;
        neuronParam.activationParam = maxActivationParam; //Could be random ?
        neuronParam.weightInputs = new List<float>();

        for (int i = 0; i <= nInputs; i++)
        {
            neuronParam.weightInputs.Add(RandomWeigth());
        }

        return neuronParam;
    }

    public static float RandomWeigth()
    {
        return Random.Range(-1f, 1f);
    }

    public static float RandomWeigthChange(float weight)
    {
        return Mathf.Clamp(weight * Random.Range(-0.1f, 0.1f), -1.0f, 1.0f);
    }
}

public struct NeuronNetwork
{
    public List<NeuronLayer> layers;
}

public struct NeuronLayer
{
    public List<NeuronParam> neuronParams;
}

public struct NeuronParam
{
    public List<float> weightInputs;
    public float activationParam;
}
