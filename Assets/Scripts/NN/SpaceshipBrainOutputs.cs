﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceshipBrainOutputs {

    public static int nOutputs = 2;
    public float targetSpeed;
    public float targetAngle;

    public SpaceshipBrainOutputs(float targetSpeed, float targetAngle)
    {
        this.targetSpeed = targetSpeed;
        this.targetAngle = targetAngle;
    }

    public SpaceshipBrainOutputs(List<float> list)
    {
        if(nOutputs != list.Count)
            Debug.LogError("ERROR outputs number in spaceship brain");

        targetSpeed = list[0];
        targetAngle = list[1];
    }

    public List<float> AsList()
    {
        List<float> outputs = new List<float>();

        outputs.Add(targetSpeed);
        outputs.Add(targetAngle);

        return outputs;
    }
}
