﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceshipBrainInputs {

    public static int nInputs = 7;
    public float forwardSpeed;
    public float forwardSpeedAngle;
    public float captor1;
	public float captor2;
	public float captor3;
	public float captor4;
	public float captor5;

    public SpaceshipBrainInputs(float forwardSpeed, float forwardSpeedAngle, float captor1, float captor2, float captor3, float captor4, float captor5)
    {
        this.forwardSpeed = forwardSpeed;
        this.forwardSpeedAngle = forwardSpeedAngle;
		this.captor1 = captor1;
		this.captor2 = captor2;
		this.captor3 = captor3;
		this.captor4 = captor4;
		this.captor5 = captor5;
	}

    public SpaceshipBrainInputs(List<float> list)
    {
        if (nInputs != list.Count)
            Debug.LogError("ERROR outputs number in spaceship brain");

        forwardSpeed = list[0];
        forwardSpeedAngle = list[1];
        captor1 = list[2];
		captor2 = list[3];
		captor3 = list[4];
		captor4 = list[5];
		captor5 = list[6];
	}

    public List<float> AsList()
    {
        List<float> inputs = new List<float>();

        inputs.Add(forwardSpeed);
        inputs.Add(forwardSpeedAngle);
        inputs.Add(captor1);
        inputs.Add(captor2);
		inputs.Add(captor3);
		inputs.Add(captor4);
		inputs.Add(captor5);

		return inputs;
    }
}
