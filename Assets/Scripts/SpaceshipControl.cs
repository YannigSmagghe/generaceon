﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceshipControl : MonoBehaviour {

    private Rigidbody2D rb;
    //spaceship speed
    public float maxSpeed = 100;
    private float targetSpeed = 0;

    //spaceship angle
    public float turnSpeed = 5;
    public float maxAngle = 30;
    private float targetAngle = 0;

    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        ApplyAngle();
        ApplySpeed();
	}

    private void ApplyAngle()
    {
        rb.AddTorque(-targetAngle * turnSpeed);
    }


    private void ApplySpeed()
    {
        rb.AddForce(-transform.up * targetSpeed);
    }

    public void SetTargetSpeed(float targetSpeed)
    {
        this.targetSpeed = Mathf.Clamp(targetSpeed, 0f, 1f) * maxSpeed;
    }


    public void SetTargetAngle(float targetAngle)
    {
        this.targetAngle = Mathf.Clamp(targetAngle, -1f, 1f) * maxAngle;
    }

    internal void ResetPosition()
    {
        transform.position = Vector3.zero;
    }
}
